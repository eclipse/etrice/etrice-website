# eTrice Website

This repository serves the contents for [eclipse.dev/etrice](https://eclipse.dev/etrice).

The contents are generated, the sources are located in [org.eclipse.etrice.doc](https://gitlab.eclipse.org/eclipse/etrice/etrice/-/tree/master/plugins/org.eclipse.etrice.doc) in the eTrice repo.
