#!/usr/bin/env sh

#
# Create a clean state to copy over new website contents
#

rm * -fr
git checkout HEAD -- CODE_OF_CONDUCT.md CONTRIBUTING.txt LICENSE NOTICE README.md SECURITY.md
